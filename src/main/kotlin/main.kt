import androidx.compose.desktop.DesktopTheme
import androidx.compose.desktop.Window
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Checkbox
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp

fun main() = Window (title = "Todo test"){
    readFile("todo.txt")

    MaterialTheme {
        DesktopTheme {
            list.forEach {
                LazyColumn(
                ) {
                    items(items = list) {
                    todoRow(it)
                    Divider()
                    }
                }
            }
        }
    }

}

@Composable
fun todoRow(todo: Todo) {
    Row {
        Checkbox(
            checked = todo.completed,
            modifier = Modifier.align(CenterVertically),
            onCheckedChange = {
                println("O was clicked")
            }
        )
        Spacer(modifier = Modifier.width(8.dp))
        Text(text = todo.priority.code, Modifier.align(CenterVertically))
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = AnnotatedString(todo.text),
            modifier = Modifier.weight(1F).align(CenterVertically),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
        Spacer(modifier = Modifier.width(8.dp))
    }
}
