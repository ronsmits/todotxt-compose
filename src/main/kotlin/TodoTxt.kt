import java.io.File
import java.util.*
import java.util.regex.Pattern

val list = mutableListOf<Todo>()
fun readFile(filename: String) {
    File(filename).forEachLine {
        list.add(TodoParser.split(it))
    }
}

fun main() {
    readFile("todo.txt")
}

internal object TodoParser {
    fun split(inputText: String?): Todo {
        if (inputText == null) {
            return Todo(Priority.NONE, "", "", false, "")
        }
        val completedMatcher = COMPLETED_PATTERN.matcher(inputText)
        val completed: Boolean
        var text: String
        if (completedMatcher.find()) {
            completed = true
            text = completedMatcher.group(2)
        } else {
            completed = false
            text = inputText
        }
        var priority: Priority = Priority.NONE
        if (!completed) {
            val prioritySplitResult = PriorityTextSplitter.split(text)
            priority = prioritySplitResult.first
            text = prioritySplitResult.second
        }
        var completedDate = ""
        var prependedDate = ""
        when {
            completed -> {
                val completedAndPrependedDatesMatcher = COMPLETED_PREPENDED_DATES_PATTERN
                    .matcher(text)
                when {
                    completedAndPrependedDatesMatcher.find() -> {
                        completedDate = completedAndPrependedDatesMatcher.group(1)
                        prependedDate = completedAndPrependedDatesMatcher.group(2)
                        text = completedAndPrependedDatesMatcher.group(3)
                    }
                    else -> {
                        val completionDateMatcher = SINGLE_DATE_PATTERN
                            .matcher(text)
                        if (completionDateMatcher.find()) {
                            completedDate = completionDateMatcher.group(1)
                            text = completionDateMatcher.group(2)
                        }
                    }
                }
            }
            else -> {
                val prependedDateMatcher = SINGLE_DATE_PATTERN.matcher(text)
                if (prependedDateMatcher.find()) {
                    text = prependedDateMatcher.group(2)
                    prependedDate = prependedDateMatcher.group(1)
                }
            }
        }
        val todo = Todo(priority, text, prependedDate, completed, completedDate)
        println(todo)
        return todo
    }

    private val COMPLETED_PATTERN = Pattern.compile("^([X,x] )(.*)")

    private val COMPLETED_PREPENDED_DATES_PATTERN = Pattern.compile("^(\\d{4}-\\d{2}-\\d{2}) (\\d{4}-\\d{2}-\\d{2}) (.*)")
    private val SINGLE_DATE_PATTERN = Pattern.compile("^(\\d{4}-\\d{2}-\\d{2}) (.*)")
}

data class Todo(
    val priority: Priority,
    val text: String,
    val prependedDate: String?,
    val completed: Boolean,
    val completedDate: String?
) {
    fun showAsTodo(): String {
        return ((if (completed) "x " else "") +
                (if (priority != Priority.NONE) "${priority.fileFormat} " else "") +
                (if (!completedDate.isNullOrEmpty()) "$completedDate " else "") +
                (if (!prependedDate.isNullOrEmpty()) "$prependedDate " else "") +
                text)
    }
}

@Suppress("unused")
enum class Priority(val code: String, val fileFormat: String) {
    NONE("-", ""), A("A", "(A)"), B("B", "(B)"), C("C", "(C)"),
    D("D", "(D)"), E("E", "(E)"), F("F", "(F)"), G("G", "(G)"),
    H("H", "(H)"), I("I", "(I)"), J("J", "(J)"), K("K", "(K)"),
    L("L", "(L)"), M("M", "(M)"), N("N", "(N)"), O("O", "(O)"),
    P("P", "(P)"), Q("Q", "(Q)"), R("R", "(R)"), S("S", "(S)"),
    T("T", "(T)"), U("U", "(U)"), V("V", "(V)"), W("W", "(W)"),
    X("X", "(X)"), Y("Y", "(Y)"), Z("Z", "(Z)");


    companion object {
        private fun reverseValues(): Array<Priority> {
            val values: Array<Priority> = values()
            val reversed = arrayOf<Priority>()
            for (i in values.indices) {
                val index = values.size - 1 - i
                reversed[index] = values[i]
            }
            return reversed
        }

        private fun range(p1: Priority, p2: Priority): List<Priority> {
            val priorities = ArrayList<Priority>()
            var add = false
            for (p in if (p1.ordinal < p2.ordinal) values() else reverseValues()) {
                if (p == p1) {
                    add = true
                }
                if (add) {
                    priorities.add(p)
                }
                if (p == p2) {
                    break
                }
            }
            return priorities
        }

        fun rangeInCode(p1: Priority, p2: Priority): List<String> {
            val priorities = range(p1, p2)
            val result: MutableList<String> = ArrayList(priorities.size)
            for (p in priorities) {
                result.add(p.code)
            }
            return result
        }

        fun inCode(priorities: Collection<Priority>): ArrayList<String> {
            val strings = ArrayList<String>()
            for (p in priorities) {
                strings.add(p.code)
            }
            return strings
        }

        fun toPriority(codes: List<String?>): ArrayList<Priority> {
            val priorities = ArrayList<Priority>()
            for (code in codes) {
                priorities.add(toPriority(code))
            }
            return priorities
        }

        fun toPriority(s: String?): Priority {
            if (s == null) {
                return NONE
            }
            for (p in values()) {
                if (p.code == s.uppercase(Locale.getDefault())) {
                    return p
                }
            }
            return NONE
        }
    }
}

object PriorityTextSplitter {
    private val PRIORITY_PATTERN = Pattern.compile("^\\(([A-Z])\\) (.*)")
    fun split(text: String?): Pair<Priority, String> {
        var internalText = text ?: return Pair(Priority.NONE, "")
        var priority: Priority = Priority.NONE
        val priorityMatcher = PRIORITY_PATTERN.matcher(internalText)
        if (priorityMatcher.find()) {
            priority = Priority.toPriority(priorityMatcher.group(1))
            internalText = priorityMatcher.group(2)
        }
        return  Pair(priority, internalText)
    }
}